<?php
namespace App\Http\Controllers;

use App\Api\Youtube;
use Illuminate\Http\Request;

class FetcherController extends Controller
{
    /**
     * Instantiate a new controller instance and verify ajax
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('isajax');
    }

    /**
     * Fetching videos from given channel username
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function fetch_channel_videos(Request $request) {
        try {
            if(!$request->has("username")) {
                return response()->json([
                    'success' => false,
                    'error' => 'No username provided'
                ]);
            }

            $yt_username = $request->input("username");
            if(empty($yt_username)) {
                return response()->json([
                    'success' => false,
                    'error' => 'Username field is empty'
                ]);
            }

            $api = new Youtube(config("app.google_api_key"));

            // Getting uploads playlist id if its not already present

            if(!$request->has("uploads_playlist_id")) {
                $doc = $api->call("channels", [
                    'forUsername' => $yt_username,
                    'part' => "contentDetails"
                ]);

                if($doc['pageInfo']['totalResults'] == "0") {
                    throw new \Exception("No such username");
                }

                $uploads_playlist_id = $doc['items'][0]['contentDetails']['relatedPlaylists']['uploads'];
            } else {
                $uploads_playlist_id = $request->input("uploads_playlist_id");
            }

            // Getting videos from uploads playlist
            $params = [
                'playlistId' => $uploads_playlist_id,
                'part' => "snippet",
                'maxResults' => 20
            ];

            if($request->has("next_page_token")) {
                $params['pageToken'] = $request->input("next_page_token");
            }

            $doc = $api->call("playlistItems", $params);

            // Adding uploads playlist id to result so we dont have to fetch it again for next pages

            $doc['uploads_playlist_id'] = $uploads_playlist_id;
            return response()->json([
                'success' => true,
                'data' => $doc
            ]);


        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'error' => 'Error occurred: ' . $e->getMessage()
            ]);
        }


    }
}