<?php

namespace App\Api;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client as GuzzleClient;

class Youtube
{
    /**
     * @var Google API key
     */
    protected $api_key;

    /**
     * Youtube API constructor.
     * @param $api_key
     * @throws \Exception
     */
    function __construct($api_key) {
        if(empty($api_key))
            throw new \Exception("Please provide Google API key");

        $this->api_key = $api_key;
    }

    /**
     * @param $method string API method
     * @param $params array Parameters
     * @return array
     * @throws \Exception
     */
    function call($method, $params) {
        $client = new GuzzleClient();

        try {
            $params = array_merge($params, [
                "key" => $this->api_key
            ]);
            $doc = $client->request('GET', "https://content.googleapis.com/youtube/v3/$method", [
                'query' => $params
            ]);

            return json_decode($doc->getBody()->getContents(), true);
        } catch (GuzzleException $e) {
            throw new \Exception("HTTP error: " . $e->getMessage());
        }
    }
}