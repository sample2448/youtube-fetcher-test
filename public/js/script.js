var items_per_page = 20;
var page = 1;

jQuery(document).ready(function ($) {
    $(".yt-fetcher-form").on('submit', function (e) {
        e.preventDefault();

        $('.videos-list').html('');

        $.ajax({
            url: '/yt-fetcher',
            type: 'POST',
            dataType: 'json',
            data: {
                username: $("#yt-username").val(),
                _token: $("#csrf-token").val()
            }
        })
        .done(function(data) {
            if(data.success) {
                render_videos_list(data.data);

                page = 1;

                var remaining_items = data.data.pageInfo.totalResults - (page * items_per_page);
                if( remaining_items > 0 ) {
                    $('#load-more').show();
                } else {
                    $('#load-more').hide();
                }

                $('#next_page_token').val(data.data.nextPageToken);
                $('#uploads_playlist_id').val(data.data.uploads_playlist_id);
            } else {
                alert(data.error);
            }
        })
        .fail(function() {
            alert("Error occurred");
        });
    });

    $('#load-more').on('click', function () {

        $.ajax({
            url: '/yt-fetcher',
            type: 'POST',
            dataType: 'json',
            data: {
                username: $("#yt-username").val(),
                uploads_playlist_id: $("#uploads_playlist_id").val(),
                next_page_token: $("#next_page_token").val(),
                _token: $("#csrf-token").val()
            }
        })
        .done(function(data) {
            if(data.success) {
                render_videos_list(data.data);

                page++;

                var remaining_items = data.data.pageInfo.totalResults - (page * items_per_page);
                if( remaining_items > 0 ) {
                    $('#load-more').show();
                } else {
                    $('#load-more').hide();
                }

                $('#next_page_token').val(data.data.nextPageToken);
            } else {
                alert(data.error);
            }
        })
        .fail(function() {
            alert("Error occurred");
        });
    });
});

function render_videos_list(data) {
    var items = data.items;
    var $videos_list = $('.videos-list');

    items.forEach(function (item, i) {

        $videos_list.append('<li class="item"><a href="https://www.youtube.com/watch?v=' + item.snippet.resourceId.videoId + '"><div class="row"><div class="video-thumbnail col-xs-2"><img src="' + item.snippet.thumbnails.default.url + '" alt=""></div><div class="video-title col-xs-10">' + item.snippet.title + '</div></div></a></li>');

    });
}