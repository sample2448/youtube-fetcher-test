<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Youtube Fetcher Test</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ config('app.url') }}/css/app.css">
        <link rel="stylesheet" href="{{ config('app.url') }}/css/style.css">
    </head>
    <body>

    <div class="panel panel-default panel-fetcher">
        <!-- Default panel contents -->
        <div class="panel-heading">YouTube Fetcher</div>
        <div class="panel-body">
            <form action="{{ config('app.url') }}/yt-fetch" class="yt-fetcher-form">
                <input type="hidden" name="csrf-token" id="csrf-token" value="{{ csrf_token() }}">
                <input type="hidden" name="uploads_playlist_id" id="uploads_playlist_id" value="">
                <input type="hidden" name="next_page_token" id="next_page_token" value="">
                <label for="yt-username">Get list of videos from channel</label>
                <input id="yt-username" type="text" class="form-control" placeholder="YouTube username">
                <input class="btn btn-primary" type="submit" value="Fetch">
            </form>
            <ul class="videos-list">

            </ul>
            <button type="button" id="load-more" class="btn btn-primary">Load more...</button>
        </div>
    </div>


    <!-- Scripts -->
    <script type="text/javascript" src="{{ config('app.url') }}/js/app.js"></script>
    <script type="text/javascript" src="{{ config('app.url') }}/js/script.js"></script>
    </body>
</html>
